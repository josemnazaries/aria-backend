class AddAdditionalFieldsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :phone_number, :string
    add_column :users, :push_notification, :boolean
    add_column :users, :email_notifications, :boolean
  end
end
