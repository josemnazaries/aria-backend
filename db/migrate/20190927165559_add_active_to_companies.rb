class AddActiveToCompanies < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :active, :boolean
  end
end
