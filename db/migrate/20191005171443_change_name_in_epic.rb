class ChangeNameInEpic < ActiveRecord::Migration[6.0]
  def change
    add_index :epics, :name, unique: true
  end
end
