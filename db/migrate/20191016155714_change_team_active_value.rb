class ChangeTeamActiveValue < ActiveRecord::Migration[6.0]
  def change
    change_column :teams, :active, :boolean, :default => true
  end
end
