class PermissionsTeams < ActiveRecord::Migration[6.0]
  def change
    remove_column :permissions, :is_create
    remove_column :permissions, :is_read
    remove_column :permissions, :is_update
    remove_column :permissions, :is_delete
    add_column :permissions, :description, :string, null: true
    add_column :permissions, :action, :string, null: false
    add_column :permissions, :subject_class, :string, null: false
    
    add_index :permissions, [:action, :subject_class], unique: true
  end
end
