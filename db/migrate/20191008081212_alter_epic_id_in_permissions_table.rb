class AlterEpicIdInPermissionsTable < ActiveRecord::Migration[6.0]
  def change
    change_column :permissions, :epic_id, :integer, null: true
  end
end
