class CreateEpics < ActiveRecord::Migration[6.0]
  def change
    create_table :epics do |t|
      t.string :name

      t.timestamps
    end
  end
end
