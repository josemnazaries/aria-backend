class RemoveTeamIdsUser < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :team_ids
  end
end
