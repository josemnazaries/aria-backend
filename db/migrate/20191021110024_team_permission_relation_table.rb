class TeamPermissionRelationTable < ActiveRecord::Migration[6.0]
  def change
    create_table :team_permissions do |t|
      t.integer :team_id
      t.integer :permission_id

      # Here comes the generated code 
      t.timestamps
    end
    remove_column :teams, :permission_ids
  end
end
