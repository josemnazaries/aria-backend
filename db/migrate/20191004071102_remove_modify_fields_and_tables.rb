class RemoveModifyFieldsAndTables < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :role_id
    remove_column :users, :team_id, :integer
    drop_table :roles
    remove_column :teams, :permissions
    add_column :teams, :permission_ids, :integer, array: true, default: []    
  end
end
