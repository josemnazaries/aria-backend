class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.string :title
      t.belongs_to :epic, null: false, foreign_key: true
      t.boolean :is_create, default: false
      t.boolean :is_read, default: true
      t.boolean :is_update, default: false
      t.boolean :is_delete, default: false

      t.timestamps
    end
  end
end
