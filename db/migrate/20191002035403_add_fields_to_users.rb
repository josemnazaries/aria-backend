class AddFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :role, foreign_key: true
    add_column :users, :team_id, :integer
    add_column :users, :is_active, :boolean, default: false
  end
end
