class ChangeCompanyIdTeams < ActiveRecord::Migration[6.0]
  def change
    change_column :teams, :company_id, "integer USING CAST(company_id AS integer)", foreign_key: true, index: true
  end
end