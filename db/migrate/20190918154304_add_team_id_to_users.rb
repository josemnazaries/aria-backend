class AddTeamIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :team_ids, :string, array: true, default: []
  end
end
