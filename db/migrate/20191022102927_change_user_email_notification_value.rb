class ChangeUserEmailNotificationValue < ActiveRecord::Migration[6.0]
  def change
    change_column :users, :email_notifications, :boolean, :default => true
  end
end
