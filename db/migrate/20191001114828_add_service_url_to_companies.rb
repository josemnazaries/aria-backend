class AddServiceUrlToCompanies < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :service_url, :string
  end
end
