class TeamUsersRelationTable < ActiveRecord::Migration[6.0]
  def change
    create_table :user_teams do |t|

      # Your code comes here
      t.integer :user_id
      t.integer :team_id

      # Here comes the generated code 
      t.timestamps
    end
  end
end
