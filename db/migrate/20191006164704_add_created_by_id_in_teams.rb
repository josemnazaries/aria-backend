class AddCreatedByIdInTeams < ActiveRecord::Migration[6.0]
  def change
    add_column :teams, :created_by_id, :integer
  end
end
