class RemoveEpic < ActiveRecord::Migration[6.0]
  def change
    remove_column :permissions, :epic_id
    drop_table :epics
  end
end
