# seeds.rb
Rake::Task['permissions:controller_action'].invoke
company = Company.create(name: 'invoke', description: 'INVOKE INC', admin_email: 'aria_superadmin@invokeinc.com', active: true)
super_admin_permission = Permission.where.not(subject_class: 'all').where.not(subject_class: %w[User Permission Team]).pluck('id')
super_team = Team.create(name: 'super_admins', description: 'Super admins', company: company)
super_admin_permission.each do |i|
  TeamPermission.create(team_id: super_team.id, permission_id: i)
end
admin_permission = Permission.where.not(subject_class: 'all').where.not(subject_class: 'Company').pluck('id')
admin_team = Team.create(name: 'admins', description: 'admins', company_id: company.id)
admin_permission.each do |i|
  TeamPermission.create(team_id: admin_team.id, permission_id: i)
end
super_user = User.create(email: company.admin_email, password: 'Atlas123!', company_id: company.id, is_active: true)
UserTeam.create(user_id: super_user.id, team_id: super_team.id)
admin_user = User.create(email: 'admin@invokeinc.com', password: 'Admin987_', company_id: company.id, is_active: true)
UserTeam.create(user_id: admin_user.id, team_id: admin_team.id)

