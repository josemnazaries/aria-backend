class EmailController < ApplicationController
  before_action :authenticate_user!, except: %i[contact_admin]
  def_param_group :contact_admin_data do
    param :email, String, desc: 'email'
  end

  def contact_admin
    user = User.find_by_email(params['email'])
    if user.present?
      company_email = user.company.admin_email
      company_name = user.company.name
      UserMailer.contact_admin(params['email'], company_email, company_name).deliver_later
      render json: { success: true }
    else
      render json: { message: 'Your user is not present in the system, please contact your Administrator' },
             status: 404
    end
  end

  def send_email_notification
    if current_user.email_notifications
      UserMailer.send_email_notification(params['to_email'], params['subject'], params['email_content']).deliver_later
    else
      render json: { message: 'Email Notification not configured' }
    end
  end

  private

  def self.permission
    "User"
  end

  def email_params
    params.permit(:email)
  end
end
