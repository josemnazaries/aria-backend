# frozen_string_literal: true

class PasswordsController < Devise::PasswordsController
  before_action :authenticate_user!, except: %i[edit update]

  prepend_before_action :require_no_authentication
  # Render the #edit only if coming from a reset password email link
  append_before_action :assert_reset_token_passed, only: :edit

  # POST /resource/password
  def create
    user = User.find_by(email: params['user']['email'])
    if user.present?
      user.send_reset_password_instructions
      render status: 200, json: { message: "Email has been sent to update password." }
    else
      render status: :unprocessable_entity, json: { message: "Email not found." }
    end
  end

  # PUT /resource/password
  def update
    self.resource = resource_class.reset_password_by_token(resource_params)
    yield resource if block_given?

    if resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)
      if Devise.sign_in_after_reset_password
        resource.after_database_authentication
        sign_in(resource_name, resource)
      end
      render status: 200, json: { message: "Password has been updated successfully." }
    else
      set_minimum_password_length
      render status: :unprocessable_entity, json: { message: resource.errors }
    end
  end

  protected

  def after_resetting_password_path_for(resource)
    Devise.sign_in_after_reset_password ? after_sign_in_path_for(resource) : new_session_path(resource_name)
  end

  # The path used after sending reset password instructions
  def after_sending_reset_password_instructions_path_for(resource_name)
    new_session_path(resource_name) if is_navigational_format?
  end

  # Check if a reset_password_token is provided in the request
  def assert_reset_token_passed
    redirect_to new_session_path(resource_name) if params[:reset_password_token].blank?
  end

  # Check if proper Lockable module methods are present & unlock strategy
  # allows to unlock resource on password reset
  def unlockable?(resource)
    resource.respond_to?(:unlock_access!) &&
      resource.respond_to?(:unlock_strategy_enabled?) &&
      resource.unlock_strategy_enabled?(:email)
  end

  def translation_scope
    'devise.passwords'
  end
end