class UsersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  respond_to :json

  def index
    @users = User.all
    render json: @users, include: %i[teams permissions]
  end

  def show
    user = User.find(params[:id])
    render json: user, include: %i[teams permissions]
  end

  def update
    resource = User.find(params[:id])
    if params[:team_ids].present?
      UserTeam.where(user_id: params[:id]).destroy_all
      params[:team_ids].each do |i|
        UserTeam.create(user_id: params[:id], team_id: i)
      end
    end
    resource.update(user_params)
    render json: resource
  end

  def destroy
    resource = User.find(params[:id])
    resource['is_active'] = false
    return unless resource

    resource.save
    render json: resource
  end

  def user_params
    params.permit(:language, :timezone, :name, :phone_number, :push_notification, :email_notifications, team_ids: [])
  end
end