class PermissionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  api :GET, 'permissions', 'all permissions'
  def index
    permission = Permission.all
    render json: permission
  end
end
