class CompaniesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def_param_group :companies_data do
    param :name, String, desc: 'name'
    param :description, String, desc: 'description'
    param :admin_email, String, desc: 'admin_email'
    param :service_url, String, desc: 'service url'
    param :active, :boolean, desc: 'active'
  end

  api :GET, 'companies', 'all companies'
  param_group :companies_data

  def index
    companies = Company.where(active: true)
    render json: companies
  end

  api :POST, 'companies', 'create a company'
  param_group :companies_data

  def create
    company = Company.new(company_params)
    company['active'] = true
    company.save

    create_team_from_company(company_params)

    render json: company.as_json(include: %i[teams users])
  end

  def create_team_from_company(params)
    company = Company.find_by(name: params['name'])
    permissionids = Permission.where.not(subject_class: 'all').where.not(subject_class: 'Company').pluck('id')
    team = company.teams.create(name: 'admins', description: params['description'], created_by_id: current_user.id, active: true)
    permissionids.each do |i|
      TeamPermission.create(team_id: team.id, permission_id: i)
    end
    user = company.users.create(email: params['admin_email'], password: "#{params['name']}123!")
    UserTeam.create(user_id: user.id, team_id: team.id)
    return unless user.present?

    user.send_reset_password_instructions
  end

  api :GET, 'companies/:id', 'retrieves a company data'
  param_group :companies_data

  def show
    @company = Company.find(params[:id])
    render json: @company
  end

  api :POST, 'companies/:id/edit', 'edits a company'
  param :id, String, desc: 'id', required: true
  param_group :companies_data

  def update; end

  api :DELETE, 'companies/:id', 'delete/deactivate a company'
  param :id, String, desc: 'id', required: true

  def destroy
    @company = Company.find(params[:id])
    @company['active'] = false
    return unless @company

    @company.save
    render json: @company
  end

  private

  def company_params
    params.permit(:name, :description, :admin_email, :service_url, :active)
  end
end
