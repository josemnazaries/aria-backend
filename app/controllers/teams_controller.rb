class TeamsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def_param_group :team_data do
    param :name, String, desc: 'name'
    param :description, String, desc: 'description'
    param :permission_ids, Array, desc: 'array of permission_ids'
  end

  def_param_group :team_edit_data do
    param :name, String, desc: 'name'
    param :description, String, desc: 'description'
    param :permission_ids, Array, desc: 'array of permission_ids'
  end

  def index
    render json: Team.all, include: %i[permissions]
  end

  api :POST, 'teams', 'create a new team'
  param_group :team_data
  def create
    if Team.find_by(name: params['name'], company_id: current_user.company_id).present?
      render json: { message: 'Team name already exist.' }
    else
      team = Team.new(name: params['name'], description: params['description'], company_id: current_user.company_id, active: true)
      if team.save
        params['permission_ids'].each do |i|
          TeamPermission.create(team_id: team.id, permission_id: i)
        end
        render json: team
      else
        # TODO: team.errors.messages
        render json: { message: 'Validation error' }
      end
    end
  end

  api :PUT, 'teams', 'Edit team'
  param :id, :number, desc: 'id', required: true
  param_group :team_edit_data
  def update
    if current_owner_team.present?
      update_team = current_owner_team.update(user_params)
      if params[:permission_ids].present?
        TeamPermission.where(team_id: params[:id]).destroy_all
        params[:permission_ids].each do |i|
          TeamPermission.create(team_id: params[:id], permission_id: i)
        end
      end
      if update_team
        render json: update_team
      else
        render json: { message: 'Validation error' }
      end
    else
      render json: { message: 'Team is not available' }
    end
  end

  api :DELETE, 'teams', 'Delete team'
  def destroy
    if current_owner_team.present? && current_owner_team.update(active: false)
      render json: current_owner_team
    else
      render json: { message: 'Team is not available.' }
    end
  end

  private

  def current_owner_team
    Team.find_by(id: params['id'], company_id: current_user.company_id)
  end

  def user_params
    params.permit(:name, :description)
  end
end
