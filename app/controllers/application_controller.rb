class ApplicationController < ActionController::API
  def render_resource(resource)
    if resource.errors.empty?
      render json: resource
    else
      validation_error(resource)
    end
  end

  def validation_error(resource)
    render json: {
      errors: [
        {
          status: '400',
          title: 'Bad Request',
          detail: resource.errors,
          code: '100'
        }
      ]
    }, status: :bad_request
  end

  rescue_from CanCan::AccessDenied do |_exception|
    render json: { message: 'You are not authorized to do this action' }
  end

  protected

  # derive the model name from the controller. egs UsersController will return User
  def self.permission
    self.to_s.gsub('Controller', '').singularize.split('::').last.constantize.name rescue nil
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  # load the permissions for the current user so that UI can be manipulated

  def load_permissions
    teams = current_user.teams
    permissions = TeamPermission.where(team_id: teams.pluck('id')).pluck("permission_id").flatten
    @current_permissions = Permission.where(id: permissions).pluck("subject_class", "action")
  end

  def configure_permitted_parameters
    added_attrs = %i[name email password password_confirmation reset_password_token]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :sign_in, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end
end
