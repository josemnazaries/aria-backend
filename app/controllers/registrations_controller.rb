class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  api :POST, 'users'
  param :email, String, desc: 'email'
  param :password, String, desc: 'password'

  def create
    if current_user.company
      teams = Team.where(id: params['team_ids']).pluck('id')
      user = build_resource(sign_up_params)
      resource.save
      teams.each do |i|
        UserTeam.create(user_id: user.id, team_id: i)
      end
      render_resource(resource)
    else
      render json: { message: 'Company does not exist.' }
    end
  end

  private

  def sign_up_params
    params.permit(:email, :password, :company_id, :name)
  end
end

