class ItemsController < ApplicationController
  include HTTParty

  api :GET, 'items', 'get all items from comm_api'
  def index
    items = self.class.get("#{ENV['COMM_API_URL']}/items")
    render json: items.body
  end

  api :GET, 'items/:id', 'get item from comm_api'
  param :id, String, desc: 'id'
  def show
    item = self.class.get("#{ENV['COMM_API_URL']}/items/#{params[:id]}")
    render json: item
  end

  def destroy
    item = self.class.delete("#{ENV['COMM_API_URL']}/items/#{params[:id]}")
    render json: item.response
  end
end
