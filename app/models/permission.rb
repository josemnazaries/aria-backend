# == Schema Information
#
# Table name: permissions
#
#  id            :bigint           not null, primary key
#  title         :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  description   :string
#  action        :string           not null
#  subject_class :string           not null
#

class Permission < ApplicationRecord
  has_many :team_permissions
  has_many :teams, through: :team_permissions

  validates_uniqueness_of :title, scope: %i[action subject_class]
end
