# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)

    # team_ids = user.team_ids.map(&:to_i)
    # permission_ids = Team.where(id: team_ids).pluck("permission_ids").flatten
    # permissions = Permission.where(id: permission_ids).pluck("subject_class", "action")
    team_ids = user.teams
    permission_ids = TeamPermission.where(team_id: team_ids.pluck('id')).pluck("permission_id")
    user_permission = Permission.where(id: permission_ids)
    user_permission.each do |i|
      if i.subject_class == "all"
        can i.action.to_sym, i.subject_class.to_sym
      else
        can i.action.to_sym, i.subject_class.constantize
      end
    end
    #     can :read, :all
    #   end
    #
    # if
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
