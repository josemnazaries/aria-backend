class Epic < ApplicationRecord
  has_many :features
  validates :name, presence: true, uniqueness: true
end