# == Schema Information
#
# Table name: companies
#
#  id          :bigint           not null, primary key
#  name        :string
#  description :text
#  admin_email :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  active      :boolean
#  service_url :string
#

class Company < ApplicationRecord
  has_many :teams
  has_many :users

  validates :name, presence: true, uniqueness: true
end
