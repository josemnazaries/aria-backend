# == Schema Information
#
# Table name: team_permissions
#
#  id            :bigint           not null, primary key
#  team_id       :integer
#  permission_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class TeamPermission < ApplicationRecord
  belongs_to :team
  belongs_to :permission
end
