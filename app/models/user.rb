# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  is_active              :boolean          default(TRUE)
#  company_id             :bigint
#  name                   :string
#  language               :string
#  timezone               :string
#  phone_number           :string
#  push_notification      :boolean
#  email_notifications    :boolean
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :jwt_authenticatable,
         jwt_revocation_strategy: JwtBlacklist

  belongs_to :company
  has_many :user_teams
  has_many :teams, -> { active }, through: :user_teams
  has_many :permissions, through: :teams

  validates :email, presence: true, uniqueness: true
  scope :get_team_name, ->(name) { Team.find_by_name(name) }
  scope :get_team_id, ->(id) { Team.where(id: id) }

  def self.send_reset_password_instructions(attributes = {})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if !recoverable.approved?
      recoverable.errors[:base] << I18n.t("devise.failure.not_approved")
    elsif recoverable.persisted?
      recoverable.send_reset_password_instructions
    end
    recoverable
  end

  protected

  def serializable_hash(options = nil)
    super(options).merge(last_sign_in_at: last_sign_in_at)
  end
end
