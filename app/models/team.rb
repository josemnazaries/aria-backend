# == Schema Information
#
# Table name: teams
#
#  id            :bigint           not null, primary key
#  name          :string
#  description   :text
#  company_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  created_by_id :integer
#  active        :boolean          default(TRUE)
#

class Team < ApplicationRecord
  belongs_to :company
  has_many :user_teams
  has_many :team_permissions
  has_many :users, through: :user_teams
  has_many :permissions, through: :team_permissions
  validates_uniqueness_of :name, scope: %i[company_id]

  scope :active, -> { where(active: true) }

  def super_admin?
    Team.find_by_name('super_admins').present?
  end
end
