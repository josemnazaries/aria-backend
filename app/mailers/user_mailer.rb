class UserMailer < ApplicationMailer
  default from: ENV['gmail_username']

  def welcome; end

  def contact_admin(user_email, company_email, company_name)
    @email = user_email
    @company_name = company_name
    mail(to: company_email, subject: 'A user is trying Login')
  end

  def send_email_notification(to_email, subject, email_content)
    @email_content = email_content
    mail(to: to_email, subject: subject)
  end
end
