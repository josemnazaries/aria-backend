require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe 'contact_admin' do
    let(:user) { create(:user) }
    let(:mail) { described_class.contact_admin(user.email, user.company.admin_email, user.company.name).deliver }

    it 'renders the subject' do
      expect(mail.subject).to eq('A user is trying Login')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.company.admin_email])
    end
  end

  describe 'send_email_notification' do
    to_email = 's.sundaramoorthyperumal@invokeinc.com'
    subject = 'Notification'
    email_content = 'Test Mail Notification'
    let(:mail) { described_class.send_email_notification(to_email, subject, email_content).deliver }

    it 'renders the subject' do
      expect(mail.subject).to eq('Notification')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([to_email])
    end
  end
end
