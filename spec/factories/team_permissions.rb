# == Schema Information
#
# Table name: team_permissions
#
#  id            :bigint           not null, primary key
#  team_id       :integer
#  permission_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :team_permission do
    team_id { Team.last.id }
    permission_id { Permission.last.id }
  end
end
