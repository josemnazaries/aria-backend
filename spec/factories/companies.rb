# == Schema Information
#
# Table name: companies
#
#  id          :bigint           not null, primary key
#  name        :string
#  description :text
#  admin_email :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  active      :boolean
#  service_url :string
#

FactoryBot.define do
  factory :company do
    id { 1 }
    name { 'invoke' }
    description { 'INVOKE INC' }
    admin_email { 'manuel.lucena@invokeinc.com' }
    service_url { 'http://example.org' }
    active { true }
  end
end
