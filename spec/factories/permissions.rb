# == Schema Information
#
# Table name: permissions
#
#  id            :bigint           not null, primary key
#  title         :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  description   :string
#  action        :string           not null
#  subject_class :string           not null
#

FactoryBot.define do
  factory :permission do
    id { 1 }
    title { 'create' }
    epic_id { null }
    description { 'create company' }
    action { 'create' }
    subject_class { 'Company' }
  end
end
