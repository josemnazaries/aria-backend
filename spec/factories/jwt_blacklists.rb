# == Schema Information
#
# Table name: jwt_blacklist
#
#  id  :bigint           not null, primary key
#  jti :string           not null
#

FactoryBot.define do
  factory :jwt_blacklist do
    jti { "MyString" }
  end
end
