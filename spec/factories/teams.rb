# == Schema Information
#
# Table name: teams
#
#  id            :bigint           not null, primary key
#  name          :string
#  description   :text
#  company_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  created_by_id :integer
#  active        :boolean          default(TRUE)
#

FactoryBot.define do
  factory :team do
    id { 4 }
    name { 'Support Team' }
    description { 'Support Team' }
    company_id { Company.last.id }
    active { true }
  end
end
