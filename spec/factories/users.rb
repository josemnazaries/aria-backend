# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  is_active              :boolean          default(TRUE)
#  company_id             :bigint
#  name                   :string
#  language               :string
#  timezone               :string
#  phone_number           :string
#  push_notification      :boolean
#  email_notifications    :boolean
#

FactoryBot.define do
  factory :user do
    email { 'aria_superadmin@invokeinc.com' }
    password { 'Test123!' }
    company { build(:company) }
  end

  factory :admin do
    email { 'manuel.lucena@invokeinc.com' }
    password { 'Urim@6840' }
    company { build(:company) }
  end
end
