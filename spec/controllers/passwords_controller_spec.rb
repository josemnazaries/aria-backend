require 'rails_helper'

RSpec.describe PasswordsController, type: :controller do
  describe 'forgot password' do
    let!(:user) { create(:user) }

    it 'forgot password', :show_in_doc do
      request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, params: { 'user[email]' => 'aria_superadmin@invokeinc.com' }
      message = ActionMailer::Base.deliveries[0].to_s
      rpt_index = message.index("reset_password_token")+"reset_password_token".length+1
      @reset_password_token = message[rpt_index...message.index("\"", rpt_index)]
      expect(response).to have_http_status(:success)
    end
  end
  describe "http methods" do
    let!(:user) { create(:user) }
    before do
      sign_in user
    end

    it 'reset password', :show_in_doc do
      request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, params: { 'user[email]' => 'aria_superadmin@invokeinc.com' }
      message = ActionMailer::Base.deliveries[0].to_s
      rpt_index = message.index("reset_password_token")+"reset_password_token".length+1
      @reset_password_token = message[rpt_index...message.index("\"", rpt_index)]
      put :update, params: { 'user[reset_password_token]' => @reset_password_token, 'user[password]'=>'123456', 'user[password_confirmation]'=>'123456' }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end

