require 'rails_helper'

RSpec.describe ItemsController, type: :controller do
  describe "items" do
    it "index" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "show" do
      get :show, params: { id: 2 }
      expect(response).to have_http_status(:success)
    end

    it 'delete' do
      delete :destroy, params: { id: 2 }
      expect(response).to have_http_status(:success)
    end
  end
end
