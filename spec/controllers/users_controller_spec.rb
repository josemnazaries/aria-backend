require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "users" do
    before do
      sign_in user
    end
    let(:user) { create(:user) }
    it "lists users", :show_in_doc do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'gets user' do
      get :show, params: { id: user.id }
      expect(response).to have_http_status(:success)
    end
  end
end
