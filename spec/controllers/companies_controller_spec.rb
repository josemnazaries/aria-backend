require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do
  describe "http methods" do
    let(:company) { build(:company) }
    let(:user) { create(:user) }

    before do
      sign_in user
    end

    it 'get index', :show_in_doc do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'show company', :show_in_doc do
      get :show, params: { id: company.id }
      expect(response).to have_http_status(:success)
    end

    it 'destroys a company', :show_in_doc do
      delete :destroy, params: { id: company.id }
      expect(response).to have_http_status(:success)
      company.active = false
      company.save
      expect(company.active).to be_falsey
    end

    it 'edits a company' do
      company.name = 'InvokeINC'
      company.service_url = 'https://example.co.uk'
      patch :update, params: { id: company.id, name: company.name, service_url: company.service_url }
      expect(response).to have_http_status(:success)
      expect(company.name).to eq('InvokeINC')
      expect(company.service_url).to eq('https://example.co.uk')
    end
  end

  describe 'creates company' do
    let(:company) { build(:company) }
    let(:user) { create(:user) }

    it 'creates company', :show_in_doc do
      sign_in user
      post :create, params: { name: 'test', description: 'test', active: true }
      expect(response).to have_http_status(:success)
      expect(Company.last.active).to be_truthy
    end
  end
end
