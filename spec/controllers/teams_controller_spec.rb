require 'rails_helper'

RSpec.describe TeamsController, type: :controller do
  describe "http methods" do
    let(:team) { create(:team) }
    let(:user) { create(:user) }

    before do
      sign_in user
    end

    it 'edits a team' do
      team.name = 'Support Team'
      team.description = 'Support Team'
      patch :update, params: { id: team.id, name: team.name, description: team.description }
      expect(response).to have_http_status(:success)
      expect(team.name).to eq('Support Team')
      expect(team.description).to eq('Support Team')
    end

    it 'destroys a team', :show_in_doc do
      delete :destroy, params: { id: team.id }
      expect(response).to have_http_status(:success)
      team.active = false
      team.save
      expect(team.active).to be_falsey
    end

    describe 'creates team' do
      let(:team) { build(:team) }
      let(:user) { create(:user) }

      it 'creates team', :show_in_doc do
        sign_in user
        post :create, params: { name: 'Support Team', description: 'Support Team', company_id: 2, permission_ids: [4], active: true }
        expect(response).to have_http_status(:success)
        expect(team.active).to be_truthy
      end
    end
  end
end
