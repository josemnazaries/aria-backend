require 'rails_helper'
RSpec.describe PermissionsController, type: :controller do
  describe "http methods" do
    let(:permission) { create(:permission) }
    let(:user) { create(:user) }

    before do
      sign_in user
    end

    it 'get index', :show_in_doc do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
end
