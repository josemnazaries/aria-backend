# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  is_active              :boolean          default(TRUE)
#  company_id             :bigint
#  name                   :string
#  language               :string
#  timezone               :string
#  phone_number           :string
#  push_notification      :boolean
#  email_notifications    :boolean
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it 'create' do
  end
  it 'read' do
  end
  it 'update' do
  end
  it 'destroy' do
  end
end
