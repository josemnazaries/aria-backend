Rails.application.routes.draw do
  apipie
  devise_for :users, controllers: {
    registrations: 'registrations',
    sessions: 'sessions',
    passwords: 'passwords'
  }
  resources :companies
  resources :users, only: %i[index show update destroy]
  resources :permissions
  resources :teams
  resources :items, only: %i[index show destroy]
  post 'contact_admin', to: 'email#contact_admin'
  post 'send_email_notification', to: 'email#send_email_notification'
end
