# config valid for current version and patch releases of Capistrano
lock "~> 3.11.1"
set :application, "acw_rails_api"
set :repo_url, "git@gitlab.com:invokeinc/rails_api.git"
set :deploy_to, "/srv/code/api_rails"
set :use_sudo, true
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
append :linked_files, "config/database.yml"
set :keep_releases, 1
set :stages, %i[staging production]
set :default_stage, "staging"
set :branch, ENV['BRANCH'] || 'develop'
