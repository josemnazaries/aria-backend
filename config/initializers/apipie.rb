Apipie.configure do |config|
  config.app_name                = "ACW_Rails_API"
  config.api_base_url            = "/"
  config.doc_base_url            = "/docs"
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
  config.translate = false
end
